#!/usr/bin/bash

Help()
{
   # Display Help
   echo "Command when run this file"
   echo
   echo "The commands are: "
   echo
   echo "f     File storage information when run spilit.sh file."
   echo "i     Information want to find. "
   echo "h     Print this Help."
   echo 
   echo "Usage: "
   echo 
   echo "           bash [file] <-command> <-command> [file-input] "
   echo
}

while getopts "f:i:h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
        \?) # incorrect option
            echo "Error: Invalid option" 
            exit;;
        f) filename=${OPTARG};;
        i) input=${OPTARG};;
    esac

done

cat $filename | while IFS="-" read -r line 
do 
    grep -o '[^-]\+$' <<<$line 
done >> epoch-time.csv && cat epoch-time.csv | while read line
do 
    date '+%Y-%m-%d' -d @"$line" 
done >> unix-time.csv && paste -d / unix-time.csv $filename > date-images.csv  
 
while read line 
do
    if [[ $line == *$input* ]]; 
    then
        echo  $line >> $input.csv 
    fi
done  < date-images.csv 
echo "New Filename: " $input.csv
rm -rf date-images.csv images.csv
