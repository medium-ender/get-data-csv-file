#!/usr/bin/bash

Help()
{
   # Display Help
   echo "Command when run this file"
   echo
   echo "The commands are: "
   echo
   echo "n     Name of file tar."
   echo "f     File to tar.gz. "
   echo "h     Print this Help."
   echo 
   echo "Usage: "
   echo 
   echo "           bash [file] <-command> <-command> [file-input] "
   echo
}

while getopts "n:f:h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
        \?) # incorrect option
            echo "Error: Invalid option" 
            exit;;
        n) name=${OPTARG};;
        f) file=${OPTARG};;
    esac

done

tar -czvf $name.tar.gz $file 