#!/usr/bin/bash

Help()
{
   # Display Help
   echo "Command when run this file"
   echo
   echo "The commands are: "
   echo
   echo "f     Input file to spilit with the command."
   echo "h     Print this Help."
   echo 
   echo "Usage: "
   echo 
   echo "           bash [file] <-command> [file-input] "
   echo
}

while getopts "f:h" option; do # 'sigle letter:' is argument flag , ' :sigle letter' is argument opt
    case $option in
        h) # display Help
            Help
            exit;;
        \?) # incorrect option
            echo "Error: Invalid option"
            exit;;
        f) filename=${OPTARG};; 
    esac
done

# use bash argument (also known as positional arguments ) 

cat $filename | while IFS=',' read id image images 
do
    [[ "$images" == *"-"* ]] && echo "$images" | sed -e 's/\"//g' -e 's/,/\n/g' 
done >> images.csv
echo 'New Filename : images.csv'



