#!/usr/bin/bash

Help()
{
   # Display Help
   echo "Command when run this file"
   echo
   echo "The commands are: "
   echo
   echo "u     File to untar."
   echo "d     Directory address save file untar."
   echo "h     Print this Help."
   echo 
   echo "Usage: "
   echo 
   echo "           bash [file] <-command> <-command> [file-input] "
   echo
}

while getopts "u:d:h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
        \?) # incorrect option
            echo "Error: Invalid option" 
            exit;;
        u) untar=${OPTARG};;
        d) dir=${OPTARG};;
    esac
done

mkdir $dir 
echo "Input file to untar : $untar " ;
tar -xzvf $untar -C ./$dir
rm -rf $untar
